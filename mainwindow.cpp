#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QFileDialog"
#include "QFile"
#include "QTextStream"
#include "QMessageBox"
#include <QtCore/qmath.h>
#include "QTime"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    ui->sixthresault->clear();
   double a = ui->firstax->text().toDouble();
   double b = ui->firstb->text().toDouble();
   double answer = linearEquation(a, b);
   ui->firstresault->setText("X = " + QString::number(answer));
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->sixthresault->clear();
    double a = ui->secondax2->text().toDouble();
    double b = ui->secondbx->text().toDouble();
    double c = ui->secondc->text().toDouble();
    if (b * b - 4 * a * c < 0)
        ui->secondresault->setText("Отсутствуют действительные корни квадратного уравнения.\n");
    else {
        QVector<double> s = squareEquation(a, b, c);
        if(fabs(s[0] - s[1]) < 0.000000001)
            ui->secondresault->setText("Корень квадратного уравнения = " + QString::number(s[0]) + "\n");
        else
            ui->secondresault->setText("Корни квадратного уравнения: " + QString::number(s[0]) + ", "
                    + QString::number(s[1]) + "\n");
    }
}


void MainWindow::on_pushButton_3_clicked()
{
    ui->sixthresault->clear();
    double a1, q;
    QString r = ui->fifthan->text();
    int n = ui->fourthn->text().toInt();
    if (r == "rand") {
        a1 = rand() % 200 - 100;
        q = rand() % 200 - 100;
    }
    else {
        a1 = ui->fourthan->text().toDouble();
        q = ui->fourthb->text().toDouble();
    }
    double s = elemArithProgress(n, a1, q);
    ui->fourthresault->setText("Арифм. прогрессия(a1 = " + QString::number(a1) + ", d = " + QString::number(q) + "): "
                                  + QString::number(n) + "-й элемент = " + QString::number(s) + "\n");
}


void MainWindow::on_pushButton_4_clicked()
{
    ui->sixthresault->clear();
    double a1, q;
    int n = ui->fifthn->text().toInt();
    QString r = ui->fifthan->text();
    if (r == "rand") {
        a1 = rand() % 200 - 100;
        q = rand() % 200 - 100;
    }
    else {
        a1 = ui->fiftha1->text().toDouble();
        q = ui->fifthan->text().toDouble();
    }
    double s = sumArithProgress(n, a1, q);
    ui->fifthresault->setText("Арифм. прогрессия(a1 = " + QString::number(a1) + ", d = " + QString::number(q) + "): "
                                  + "сумма по " + QString::number(n) + "-й элемент = " + QString::number(s) + "\n");
}

void MainWindow::on_pushButton_5_clicked()
{    
    ui->sixthresault->clear();
    double b1, d;
    int n = ui->sixthn->text().toInt();
    QString r = ui->sixthq->text();
    if (r == "rand") {
        b1 = rand() % 200 - 100;
        d = rand() % 200 - 100;
    }
    else {
        b1 = ui->sixthb1->text().toDouble();
        d = ui->sixthq->text().toDouble();
    }
    double s = sumGeomProgress(n, b1, d);
    ui->sixthresault->setText("Геом. прогрессия(b1 = " + QString::number(b1) + ", q = "
                                  + QString::number(d) + "): " + "сумма по " + QString::number(n)
                                  + "-й элемент = " + QString::number(s) + "\n");
}

void MainWindow::on_pushButton_6_clicked()
{
    ui->sixthresault->clear();
    double a = ui->thirda->text().toDouble();
    double b = ui->thirdb->text().toDouble();
    double c = ui->thirdc->text().toDouble();
    double d = ui->thirdd->text().toDouble();
    QVector<double> s = cubeEquation(a, b, c, d);
    if(s.size() == 1) {
        ui->thirdresault->setText("Корень кубического уравнения = " + QString::number(s[0]) + "\n");
    }
    else {
        ui->thirdresault->setText("Корни кубического уравнения: " + QString::number(s[0]));
        for (int i = 1; i < s.size(); ++i)
            ui->thirdresault->setText(", " + QString::number(s[i]));
        ui->thirdresault->setText("\n");
    }
}
