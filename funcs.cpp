#include "funcs.h"


double linearEquation(double a, double b)
{
    if (fabs(a) < 0.000000001)
        return 0;
    else
        return -b/a;
}

QVector<double> squareEquation(double a, double b, double c)
{
    QVector<double> x;
    double D = b * b - 4 * a * c;
    if (D < 0)
        return x;
    else {
        x = {(-b+sqrt(D))/(2 * a), (-b-sqrt(D))/(2 * a)};
        return x;
    }
}

QVector<double> cubeEquation(double a1, double b1, double c1, double d1)
{
    QVector<double> x;
    double a = b1/a1, b = c1/a1, c = d1/a1;
    double q,r,r2,q3;
      q=(a*a-3.*b)/9.; r=(a*(2.*a*a-9.*b)+27.*c)/54.;
      r2=r*r; q3=q*q*q;
      if(r2<q3) {
        double t=acos(r/sqrt(q3))/3.;
        x.push_back(-2.*sqrt(q)*cos(t)-a/3.);
        x.push_back(-2.*sqrt(q)*cos(t+2*M_PI/3.)-a/3.);
        x.push_back(-2.*sqrt(q)*cos(t-2*M_PI/3.)-a/3.);
      }
      else {
        double aa,bb;
        aa=-pow(fabs(r)+sqrt(r2-q3),1./3.);
        if (r < 0)
            aa = -aa;
        if (fabs(aa) > 0.000000001)
            bb=q/aa;
        else
            bb=0.;
        x.push_back(aa+bb-a/3);
        if (fabs(aa-bb) < 0.000000001)
            x.push_back(-aa-a/3);
      }
      return x;
}

double elemArithProgress(int n, double a, double d)
{
    return a + (n-1) * d;
}

double sumArithProgress(int n, double a, double d)
{
    return ((2 * a + (n-1) * d) * n) / 2;
}

double sumGeomProgress(int n, double b, double q)
{
    return b * (1 - pow(q, n)) / (1 - q);
}

