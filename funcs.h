#ifndef FUNCS_H
#define FUNCS_H
#include <QVector>
#include <QtMath>


double linearEquation(double a, double b);

QVector<double> squareEquation(double a, double b, double c);

QVector<double> cubeEquation(double a1, double b1, double c1, double d1);

double elemArithProgress(int n, double a, double d);

double sumArithProgress(int n, double a, double d);

double sumGeomProgress(int n, double b, double q);



#endif // FUNCS_H
