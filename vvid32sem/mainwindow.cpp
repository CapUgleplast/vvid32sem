#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QFileDialog"
#include "QFile"
#include "QTextStream"
#include "QMessageBox"
#include <QtCore/qmath.h>
#include "QTime"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
   float a = ui->firstax->text().toFloat();
   float b = ui->firstb->text().toFloat();
   float answer;
   answer = -b/a;
   ui->firstresault->setText("X = " + QString::number(answer));
}

void MainWindow::on_pushButton_2_clicked()
{
    float a = ui->secondax2->text().toFloat();
    float b = ui->secondbx->text().toFloat();
    float c = ui->secondc->text().toFloat();
    float D;
    float x1;
    float x2;
    D = b*b - 4*a*c;
    if (D == 0){
        x1 = -b/2*a;
         ui->secondresault->setText("X1 = " + QString::number(x1));
    }
    else if (D < 0){
        ui->secondresault->setText("Корней не существует");
   }
    else{
    x1 = (-b + sqrt(D))/2*a;
    x2 = (-b - sqrt(D))/2*a;
    ui->secondresault->setText("X1 = " + QString::number(x1) + " X2 = " + QString::number(x2));}
}


void MainWindow::on_pushButton_3_clicked()
{
    float a1 = ui->fourthan->text().toFloat();
    QString b = ui->fourthb->text();
    float q = ui->fourthb->text().toFloat();
    float n = ui->fourthn->text().toFloat();
    float answer;
    QTime midnight(0,0,0);
    qsrand(midnight.secsTo(QTime::currentTime()));
    if(b == "rand"){
        q = qrand();
     answer = a1 + (n-1)*q;
    }else {
    q = b.toFloat();
    answer = a1 + (n-1)*q;
}

    ui->fourthresault->setText("A(n) = " + QString::number(answer));

}


void MainWindow::on_pushButton_4_clicked()
{
    float a1 = ui->fiftha1->text().toFloat();
    QString b = ui->fifthan->text();
    float q = ui->fifthan->text().toFloat();
    float n = ui->fifthn->text().toFloat();
    float answer;
    QTime midnight(0,0,0);
    qsrand(midnight.secsTo(QTime::currentTime()));
    if(b == "rand"){
        q = qrand();
        answer = ((2*a1 + q*(n-1)))/2*n;
    }else {
    q = b.toFloat();
    answer = ((2*a1 + q*(n-1)))/2*n;
}
    ui->fifthresault->setText("S(n) = " + QString::number(answer));

}

void MainWindow::on_pushButton_5_clicked()
{
    float b1 = ui->sixthb1->text().toFloat();
    float b2 = ui->sixthb2->text().toFloat();
    QString b = ui->sixthq->text();
    float d = ui->sixthq->text().toFloat();
    float n = ui->sixthn->text().toFloat();
    float answer;
    QTime midnight(0,0,0);
    qsrand(midnight.secsTo(QTime::currentTime()));
    if (d == 0)
    {
        d = b2/b1;
        answer = (b1*(qPow(d, n) - 1))/(d - 1);
        ui->sixthresault->setText("S(n) = " + QString::number(answer));
    }
    if(b2 == 0)
    {
        if(b == "rand"){
            d = qrand();
            b2 = b1*qPow(d, n - 1);
            answer = (b1*(qPow(d, n) - 1))/(d - 1);
            ui->sixthresault->setText("S(n) = " + QString::number(answer));
        }else {
            d = b.toFloat();
            b2 = b1*qPow(d, n - 1);
            answer = (b1*(qPow(d, n) - 1))/(d - 1);
            ui->sixthresault->setText("S(n) = " + QString::number(answer));
        }
    }
    if((d == 0 && b2 == 0) || n == 0)
        ui->sixthresault->setText("Не хватает данных");

}

void MainWindow::on_pushButton_6_clicked()
{
    ui->thirdresault->clear();
    float a = ui->thirda->text().toInt();
    QString s;
    float b = ui->thirdb->text().toInt();
    float x[3];
    float c = ui->thirdc->text().toInt();
    float d = ui->thirdd->text().toInt();
    float Q = (a*a - 3*b)/9;
    float R = (2*a*a*a -9*a*b + 27*c)/54;
    if (R*R <= Q*Q*Q)
    {
    float t = acos(R/sqrt(Q*Q*Q))/3;
    x[0] = 2*sqrt(Q)*cos(t)-a/3;
    x[1] = -2*sqrt(Q)*cos(t+(2*3.1415/3))-a/3;
    x[2] = -2*sqrt(Q)*cos(t-(2*3.1415/3))-a/3;

    for (int i = 0;i<3;i++)
    {
    s = QString::number(x[i]);
    ui->thirdresault->setText(s);


    }

    }
    else
    {
    float t = acos(R/sqrt(Q*Q*Q))/3;
    x[0] = 2*sqrt(Q)*cos(t)-a/3;
    x[1] = -2*sqrt(Q)*cos(t+(2*3.1415/3))-a/3;
    x[2] = -2*sqrt(Q)*cos(t-(2*3.1415/3))-a/3;

    for (int i = 0;i<3;i++)
    {
   // s = QString::number(x[i]);
    ui->thirdresault->append(QString::number(x[i]) + " ");
    }
    }

}
